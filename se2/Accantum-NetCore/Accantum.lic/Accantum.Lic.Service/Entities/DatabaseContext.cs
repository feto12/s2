﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;


namespace Accantum.Lic.Service.Entities
{
    public class DatabaseContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Partner> Partners { get; set; }
        public DbSet<Admin> Admins { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        
            string connectionString ="Data Source= INF-LIZENZ-SS19.fh-rosenheim.priv\\SQLEXPRESS;" +"User Instance=true;" +"User Id=;" +"Password=;" +"AttachDbFilename=|DataDirectory|DataBaseName.mdf;";
                       
            optionsBuilder.UseSqlServer(connectionString, providerOptions => providerOptions.CommandTimeout(60)).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
    }
}
