﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accantum.Lic.Service.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public int ZIP { get; set; }

        public int Housenumber { get; set; }

        public string EMail { get; set; }

        public string Phone { get; set; }

        public string Role { get; set; }

        public bool Blocked { get; set; }

        public int CreatorId { get; set; }
    }
}
