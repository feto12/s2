﻿using Accantum.Lic.Service.Dtos;
using Accantum.Lic.Service.Entities;
using AutoMapper;


namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}