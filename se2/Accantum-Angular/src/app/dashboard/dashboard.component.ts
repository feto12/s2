import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  LineChart=[];
  constructor() { }

  ngOnInit() {

       // Line chart:
this.LineChart = new Chart('lineChart', {
  type: 'line',
data: {
 labels: ["Jan", "Feb", "März", "April", "Mai", "Juni","Juli","Aug","Sep","Okt","Nov","Dec"],
 datasets: [{
     label: 'Anzahl der verkauften Lizenzen',
     data: [20,40 , 70, 120, 160, 170,150,160,190,185,195,200],
     fill:false,
     lineTension:0.2,
     borderColor:"blue",
     borderWidth: 1
 }]
}, 
options: {
 title:{
     text:"2019",
     display:true
 },
 scales: {
     yAxes: [{
         ticks: {
             beginAtZero:true
         }
     }]
 }
}
});



       // Line chart:
       this.LineChart = new Chart('lineChart1', {
        type: 'line',
      data: {
       labels: ["Jan", "Feb", "März", "April", "Mai", "Juni","Juli","Aug","Sep","Okt","Nov","Dec"],
       datasets: [{
           label: 'Anzahl Partner',
           data: [30,56 , 20, 40, 60, 80, 110, 100, 105 ,120, 110,130],
           fill:false,
           lineTension:0.2,
           borderColor:"green",
           borderWidth: 1
       }]
      }, 
      options: {
       title:{
           text:"2019",
           display:true
       },
       scales: {
           yAxes: [{
               ticks: {
                   beginAtZero:true
               }
           }]
       }
      }
      });
      
      
      

       // Line chart:
       this.LineChart = new Chart('lineChart2', {
        type: 'line',
      data: {
       labels: ["Jan", "Feb", "März", "April", "Mai", "Juni","Juli","Aug","Sep","Okt","Nov","Dec"],
       datasets: [{
           label: 'Anzahl Kunden',
           data: [10,20 , 30, 50, 45, 60, 70, 80, 75, 90, 95,110],
           fill:false,
           lineTension:0.2,
           borderColor:"red",
           borderWidth: 1
       }]
      }, 
      options: {
       title:{
           text:"2019",
           display:true
       },
       scales: {
           yAxes: [{
               ticks: {
                   beginAtZero:true
               }
           }]
       }
      }
      });


  }  
}
