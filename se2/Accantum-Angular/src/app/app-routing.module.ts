import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from 'src/app/layout/layout.component';
import { LoginComponent } from 'src/app/login/login.component';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { CustomerComponent } from 'src/app/customer/customer.component';
import { PageNotFoundComponent } from 'src/app/page-not-found/page-not-found.component';
import { LicenseConfiguratorComponent } from 'src/app/license-configurator/license-configurator.component';
import { LicenseAdministrationComponent } from 'src/app/license-administration/license-administration.component';
import { AccantumComponent } from './accantum/accantum.component';
import { AdminCreateComponent } from './accantum/admin-create/admin-create.component';
import { CommonModule } from '@angular/common';
import { AdminDetailComponent } from './admin-detail/admin-detail.component';
import { PartnerComponent } from './partner/partner.component';
import { AdminsComponent } from './accantum/admins/admins.component';
import { PartnerDetailComponent } from './partner/partner-detail/partner-detail.component';
import { CreatePartnerComponent } from './partner/create-partner/create-partner.component';
import { CustomerDetailComponent } from './customer/customer-detail/customer-detail.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { LicenseCreateComponent } from './license-administration/license-create/license-create.component';
import { componentFactoryName } from '@angular/compiler';
import { LicenseDetailComponent } from './license-administration/license-detail/license-detail.component';
import { AuthGuard } from './guards/auth.guard';



const routes: Routes = [

  { path: 'login', component: LoginComponent },

  {
    path: '',
    component: LayoutComponent, canActivate: [AuthGuard],
    children: [
      { path: 'dash', component: DashboardComponent, canActivate: [AuthGuard] },
      { path: 'customers', component: CustomerComponent, canActivate: [AuthGuard]  },
      { path: 'license-administration', component: LicenseAdministrationComponent, canActivate: [AuthGuard]  },
      { path: 'license-configurator', component: LicenseConfiguratorComponent, canActivate: [AuthGuard]  },
      { path: 'admin-detail/:id', component: AdminDetailComponent, canActivate: [AuthGuard]  },
      { path: 'accantum', component: AccantumComponent, canActivate: [AuthGuard] },
      { path: 'accantum/create', component: AdminCreateComponent, canActivate: [AuthGuard] },
      { path: 'partner', component: PartnerComponent, canActivate: [AuthGuard]  },
      { path: 'admin-create' , component: AdminCreateComponent, canActivate: [AuthGuard]  },
      { path: 'admins' , component: AdminsComponent, canActivate: [AuthGuard]  },
      { path: 'partner-detail/:id', component: PartnerDetailComponent, canActivate: [AuthGuard] },
      { path: 'create-partner', component: CreatePartnerComponent, canActivate: [AuthGuard]  },
      { path: 'customer-detail/:id', component: CustomerDetailComponent, canActivate: [AuthGuard] },
      { path: 'create-customer', component: CreateCustomerComponent, canActivate: [AuthGuard]  },
      { path: 'license-create', component: LicenseCreateComponent, canActivate: [AuthGuard]  },
      { path: 'license-detail/:id', component: LicenseDetailComponent, canActivate: [AuthGuard]  }

    ]
  },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
