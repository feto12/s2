import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AdminService } from '../shared/services/admin.service';
import { Admin } from '../shared/models/admin';

@Component({
  selector: 'app-admin-detail',
  templateUrl: './admin-detail.component.html',
  styleUrls: ['./admin-detail.component.css']
})
export class AdminDetailComponent implements OnInit {

  @Input() admin: Admin;

  block: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private adminService: AdminService,
    private location: Location
  ) { }

  ngOnInit() :void {
    this.getAdmin();
  }

  //Show detail of an specific admin
  getAdmin(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.adminService.getAdminByID(id)
    .subscribe(admin => this.admin = admin);
  }
  //go to the previous page
  goBack(): void {
    this.location.back();
  }
  //change the Data of an specific admin
  save(): void {
    this.adminService.updateAdminByID(this.admin, this.admin.id)
    .subscribe(() => this.goBack());
  }

  //block admin 
  blockAdmin(): void {
    if(this.admin.blocked == false){
    this.adminService.blockAdmin(this.admin, this.admin.id, this.admin.blocked =true )
    .subscribe(() => console.log(this.admin.blocked));
  }else {
    this.adminService.blockAdmin(this.admin, this.admin.id, this.admin.blocked =false )
    .subscribe(() => console.log(this.admin.blocked));

    }
  }

  blockStatus(btn): void {
    if(this.admin.blocked == false){
       btn.value= "Sperren"; 
     }else{
       btn.value= "Entsperren";
     }
  }

  deleteAdminByID(id: number){
    return this.adminService.deleteAdmin(id)
    .subscribe((admin:Admin) => {
      console.log('Admin deleted', admin, this.goBack());
    },
    error  => {
      console.log("Error", error); 
    });
  }  

}
