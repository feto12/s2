import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Customer } from 'src/app/shared/models/customer.model';
import { CustomerService } from 'src/app/shared/services/customer.service';


@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
 
  @Input() customer: Customer;
  form: FormGroup;
  btn: string;
  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private location: Location,
    private formBuilder: FormBuilder,
  ) { }

  

  ngOnInit() :void {
    this.getCustomer(); 
  
    }

  //Show detail of an specific customer
  getCustomer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.customerService.getCustomerByID(id)
    .subscribe(customer => this.customer = customer);
  
  }
  //go to the previous page
  goBack(): void {
    this.location.back();
  }
  //change the Data of an specific customer
  save(): void {
    this.customerService.updateCustomerByID(this.customer, this.customer.id)
    .subscribe(() => this.goBack());
  }

   //block admin 
   blockCustomer(): void {
     if(this.customer.blocked == false){
    this.customerService.blockCustomer(this.customer, this.customer.id, this.customer.blocked =true )
    .subscribe(() => console.log(this.customer.blocked) );
    
     }else{
      this.customerService.blockCustomer(this.customer, this.customer.id, this.customer.blocked =false )
      .subscribe(() => console.log(this.customer.blocked) );
     }

  }
 

  blockStatus(btn): void {
    if(this.customer.blocked == false){
       btn.value= "Sperren"; 
     }else{
       btn.value= "Entsperren";
     }
  }
  
 

  deleteCustomerByID(id: number){
    return this.customerService.deleteCustomer(id)
    .subscribe((customer:Customer) => {
      console.log('Customer deleted', customer, this.goBack());
    },
    error  => {
      console.log("Error", error);
    });
  }  

}
