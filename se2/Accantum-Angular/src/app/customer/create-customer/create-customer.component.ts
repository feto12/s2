import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Customer } from 'src/app/shared/models/customer.model';
import { CustomerService } from 'src/app/shared/services/customer.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {

  form: FormGroup;
  customers: Customer[];
  Customer: Customer;
  Role = "Customer"; 

  constructor(
    private customerservice: CustomerService, 
    private formBuilder: FormBuilder,
    private location: Location
    ) { }

  ngOnInit() {
    this.customerservice.getCustomers()
    .subscribe(data => {this.customers = data
    console.log(this.customers)
    })
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      lastName: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      password: ['', Validators.required, ],
      street: ['', [Validators.required , Validators.pattern(/^[a-zA-Z0-9ßöäü.]+$/ ) ]],
      city: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      zip: ['', [Validators.required, Validators.minLength(4)]],
      eMail: ['', [Validators.required, Validators.pattern(  /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ )] ],
      phone: ['', [Validators.required, Validators.pattern( /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]],
      role: [ this.Role ]
      
    });
  }

  
    
  //Post Customer
  createNewCustomer(Customer: Customer){
    Customer = this.form.value;
    return this.customerservice.createCustomer(Customer)
    .subscribe((data) => {
      console.log('New Customer created', this.goBack() );
    },
    error  => {
      console.log("Error", error);
    });
  
  }
 
  goBack(): void {
    this.location.back();
  }

}
