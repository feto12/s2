import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/shared/services/customer.service';
import { Customer } from '../shared/models/customer.model';
import { LoginService, CurrentUser } from '../login/login.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {


  form: FormGroup;
  Customers: Customer[];
  Customer: Customer;
  currentUser: CurrentUser;


  constructor(private loginService: LoginService, private customerService: CustomerService, private formBuilder: FormBuilder) { 
    this.currentUser = loginService.getCurrentUser();
  }

  ngOnInit() {

    this.getCustomers();

  }


  // Get all Customers
  getCustomers() {
    const currentUser = this.loginService.getCurrentUser();
    const isAdminUser = ['Partner', 'Customer'].indexOf(currentUser.role) === -1;
    return this.customerService.getCustomers()
      .subscribe((data: Customer[]) => {
        this.Customers = data;
        // Iteriere über das Array und füge nur User mit Rolle = Customer ein!
        this.Customers = this.Customers.filter(rolleCustomer =>
          rolleCustomer.role === 'Customer'
          && (isAdminUser || rolleCustomer.id === currentUser.id || rolleCustomer.creatorId === currentUser.id)
        );
        console.log(this.Customers);
      }, error => {
        console.log('Error', error);
      });
  }

  // Get Customer by id
  getCustomerByID(Id: number) {
    return this.customerService.getCustomerByID(Id)
      .subscribe((Customer: Customer) => {
        this.Customer = Customer;
      },
        error => {
          console.log('Error', error);
        });
  }




}
