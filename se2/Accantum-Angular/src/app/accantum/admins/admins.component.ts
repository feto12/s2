import { Component, OnInit, Input } from '@angular/core';
import { Admin } from 'src/app/shared/models/admin';
import { AdminService } from 'src/app/shared/services/admin.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/login/login.service';


@Component({
  selector: 'app-admins',
  templateUrl: './admins.component.html',
  styleUrls: ['./admins.component.css']
})
export class AdminsComponent implements OnInit {
  form: FormGroup;
  // user: Observable<Admin>;
  // Array with Admin


  admins: Admin[];
  admin: Admin;
  roleAdmin: Admin;



  constructor(private loginService: LoginService, private adminService: AdminService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getAdmins();
  }

  // Get all admins
  getAdmins() {
    const currentUser = this.loginService.getCurrentUser();
    const isAdminUser = ['Partner', 'Customer'].indexOf(currentUser.role) === -1;
    return this.adminService.getAdmins()
      .subscribe((data: Admin[]) => {
        console.log(data);

        this.admins = data;
        // Iteriere über das Array und füge nur User mit Rolle = Admin ein!
        this.admins = this.admins.filter(rollePartner =>
          rollePartner.role === 'Admin'
          && (isAdminUser || rollePartner.id === currentUser.id || rollePartner.creatorId === currentUser.id)
        );
      }, error => {
        console.log('Error', error)
      });
  }

  // Get Admin by id
  getAdminByID(Id: number) {
    return this.adminService.getAdminByID(Id)
      .subscribe((admin: Admin) => {
        this.admin = admin;
      },
        error => {
          console.log('Error', error);
        });
  }

}


