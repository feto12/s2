import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccantumComponent } from './accantum.component';

describe('AccantumComponent', () => {
  let component: AccantumComponent;
  let fixture: ComponentFixture<AccantumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccantumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccantumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
