import { Component, OnInit, Input } from '@angular/core';
import { Admin } from '../shared/models/admin';
import { AdminService } from '../shared/services/admin.service';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-accantum',
  templateUrl: './accantum.component.html',
  styleUrls: ['./accantum.component.css']
})
export class AccantumComponent implements OnInit {
  
  form: FormGroup;  
  admins: Admin[];
  admin: Admin;
  Role: String = "Admin";


  constructor(
    private adminService: AdminService, 
    private formBuilder: FormBuilder,
    private location: Location
    ) { }



  ngOnInit() {
    this.form = this.formBuilder.group({
      FirstName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      LastName: ['', [Validators.required, Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      password: ['', Validators.required],
      Street: ['', [Validators.required , Validators.pattern(/^[a-zA-Z0-9ßöäü.]+$/ ) ]],
      City: ['',  [Validators.required, Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      ZIP: ['', [Validators.required, Validators.minLength(4)]],
      EMail: ['', [Validators.required, Validators.pattern(  /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ )] ],
      Phone: ['', [Validators.required, Validators.pattern( /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]],
      Role: [ this.Role ]
      
    }); 
  }
    
  //Post Admin
  createNewAdmin(admin: Admin){
    admin = this.form.value;
    return this.adminService.createAdmin(admin)
    .subscribe((data) => {
      console.log('New Admin created', this.goBack() );
    },
    error  => {
      console.log("Error", error);
    });
  }

  goBack(): void {
    this.location.back();
  }
}
