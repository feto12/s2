import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseConfiguratorComponent } from './license-configurator.component';

describe('LicenseConfiguratorComponent', () => {
  let component: LicenseConfiguratorComponent;
  let fixture: ComponentFixture<LicenseConfiguratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseConfiguratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseConfiguratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
