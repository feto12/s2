import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NotificationService } from './shared/components/notification/notification.service';
import { startWith, delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'accantum-license-frontend';

  notificationState: Observable<boolean>;

  constructor(
    private notificationService: NotificationService,
) {}

ngOnInit(): void {
 // this.notificationState = this.notificationService.notificationState.pipe( startWith(null), delay(0));
   
}

}
