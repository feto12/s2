import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from '../shared/services/admin.service';
import { Admin } from '../shared/models/admin';
import { LoginService, CurrentUser } from '../login/login.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  smallSidebar = true;
  admins: Admin[];
  admin: Admin;
  currentUser: CurrentUser;
  constructor(private loginService: LoginService, private router: Router) { 
    this.currentUser = this.loginService.getCurrentUser();
    console.log(this.currentUser);
  }

  ngOnInit() {
    
  }


  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  toggleSidebar() {
    this.smallSidebar = !this.smallSidebar;
  }

}
