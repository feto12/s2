import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Configuration } from './app.constants';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { LicenseConfiguratorComponent } from './license-configurator/license-configurator.component';
import { LicenseAdministrationComponent } from './license-administration/license-administration.component';
import { CustomerComponent } from './customer/customer.component';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { library } from '@fortawesome/fontawesome-svg-core';

import {
  faSignOutAlt,
  faPlus,
  faSave,
  faBars,
  faHome,
  faUsers,
  faBalanceScale } from '@fortawesome/free-solid-svg-icons';
import { AccantumComponent } from './accantum/accantum.component';
import { AdminDetailComponent } from './admin-detail/admin-detail.component';
import { AdminSearchComponent } from './admin-search/admin-search.component';
import { NotificationComponent } from './shared/components/notification/notification.component';
import { PartnerComponent } from './partner/partner.component';
import { AdminCreateComponent } from './accantum/admin-create/admin-create.component';
import { AdminsComponent } from './accantum/admins/admins.component';
import { PartnerDetailComponent } from './partner/partner-detail/partner-detail.component';
import { CreatePartnerComponent } from './partner/create-partner/create-partner.component';
import { CreateCustomerComponent } from './customer/create-customer/create-customer.component';
import { CustomerDetailComponent } from './customer/customer-detail/customer-detail.component';
import { LicenseCreateComponent } from './license-administration/license-create/license-create.component';
import { LicenseDetailComponent } from './license-administration/license-detail/license-detail.component';


library.add(
  faSignOutAlt,
  faPlus,
  faSave,
  faBars,
  faHome,
  faUsers,
  faBalanceScale
);

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    LicenseConfiguratorComponent,
    LicenseAdministrationComponent,
    CustomerComponent,
    LayoutComponent,
    DashboardComponent,
    LoginComponent,
    PageNotFoundComponent,
    AccantumComponent,
    AdminDetailComponent,
    AdminSearchComponent,
    NotificationComponent,
    PartnerComponent,
    AdminCreateComponent,
    AdminsComponent,
    PartnerDetailComponent,
    CreatePartnerComponent,
    CreateCustomerComponent,
    CustomerDetailComponent,
    LicenseCreateComponent,
    LicenseDetailComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    Configuration
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
