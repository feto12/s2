import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { LicenseService } from 'src/app/shared/services/license.service';
import { License } from 'src/app/shared/models/license';


@Component({
  selector: 'app-license-detail',
  templateUrl: './license-detail.component.html',
  styleUrls: ['./license-detail.component.css']
})
export class LicenseDetailComponent implements OnInit {

  @Input() license: License;

  constructor(
    private route: ActivatedRoute,
    private licenseService: LicenseService,
    private location: Location

  ) { }

  ngOnInit() {
  }


   //Show detail of an specific partner
   getLicense(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.licenseService.getLicenseByID(id)
    .subscribe(license => this.license = license);
    console.log(this.license);
  }
  //go to the previous page
  goBack(): void {
    this.location.back();
  }
  //change the Data of an specific partner
  save(): void {
    this.licenseService.updateLicenseByID(this.license, this.license.id)
    .subscribe(() => this.goBack());
  }

  /*
  deleteLicenseByID(id: number){
    return this.licenseService.licensePartner(id)
    .subscribe((license:License) => {
      console.log('License deleted', license, this.goBack());
    },
    error  => {
      console.log("Error", error);
    });
  }  
 */


}
