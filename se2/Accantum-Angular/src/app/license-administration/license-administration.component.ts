import { Component, OnInit } from '@angular/core';
import { License } from 'src/app/shared/models/license';
import { LicenseService } from 'src/app/shared/services/license.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';




@Component({
  selector: 'app-license-administration',
  templateUrl: './license-administration.component.html',
  styleUrls: ['./license-administration.component.css']
})
export class LicenseAdministrationComponent implements OnInit {

  form: FormGroup;
  licenses: License[];
  license: License;


  constructor(private licenseService: LicenseService , private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getLicenses(); 
  }



  //Get all licenses 
  getLicenses() {
    return this.licenseService.getLicenses()
    .subscribe( (data: License[]) => {
      this.licenses = data;
      console.log(this.licenses);

 
  }, error => {
      console.log("Error", error)
    }); 
  }

  //Get license by id
  getlicenseByID(Id: number){
    return this.licenseService.getLicenseByID(Id)
    .subscribe((license: License) => {
      this.license = license;
    },
    error  => {
      console.log("Error", error);
    });
  }

}


