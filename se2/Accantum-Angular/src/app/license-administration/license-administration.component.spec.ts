import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LicenseAdministrationComponent } from './license-administration.component';

describe('LicenseAdministrationComponent', () => {
  let component: LicenseAdministrationComponent;
  let fixture: ComponentFixture<LicenseAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicenseAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LicenseAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
