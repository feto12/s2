import { Component, OnInit } from '@angular/core';
import { License } from 'src/app/shared/models/license';
import { LicenseService } from 'src/app/shared/services/license.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-license-create',
  templateUrl: './license-create.component.html',
  styleUrls: ['./license-create.component.css']
})
export class LicenseCreateComponent implements OnInit {

  form: FormGroup;
  licenses: License[];
  license: License;
  

  constructor(private licenseService: LicenseService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      
      //Kundendaten
      accantumNr: ['', Validators.required],
      firstName: ['',  [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      lastName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      street: ['',  [Validators.required,Validators.pattern(/^[a-zA-Z0-9ßöäü.]+$/ ) ]],
      zip: ['', [Validators.required, Validators.minLength(4)]],
      city: ['',  [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      country: ['',  [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      email1: ['', [Validators.required, Validators.pattern(  /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ )] ],
      email2: ['', [Validators.required, Validators.pattern(  /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ )] ],
      phone: ['', [Validators.required, Validators.pattern( /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]],
      fax: ['',  [Validators.required, Validators.pattern( /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]],
  
      //Anwendungsdaten
      AbbyyLicenseKind:['', Validators.required],
      AbbyyLicenseType: ['', Validators.required],
      AbbyyLicenseSize:['', [Validators.required, Validators.pattern( /^[0-9]\d*$/) ]],
      AbbyyVersion:['', Validators.required],
      expiryDate:['', [Validators.required, Validators.pattern( /^[0-9]{2}.[0-9]{2}.[0-9]{4}$/) ]],
      
      //Benutzerdaten
      lightUser: ['', [Validators.required, Validators.pattern( /^[0-9]\d*$/) ]],
      fullUser:  ['',  [Validators.required, Validators.pattern( /^[0-9]\d*$/) ]],
      totalUser: ['',  [Validators.required, Validators.pattern( /^[0-9]\d*$/) ]],
      language: ['', Validators.required],
      outputFormat: ['', Validators.required], 
      receiptDate: ['', [Validators.required, Validators.pattern( /^[0-9]{2}.[0-9]{2}.[0-9]{4}$/) ]],

    });
  }
    //Post License
    createNewLicense(license: License){
      license = this.form.value;
      return this.licenseService.createLicense(license)
      .subscribe((data) => {
        console.log(this.license);
        alert("button was cklicked");
        console.log('New License created', location.reload() );
      },
      error  => {
        console.log("Error", error);
      });
    }

}
