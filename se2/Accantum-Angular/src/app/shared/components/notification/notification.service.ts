import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Notification } from './notification.model';


type Severities = 'success' | 'info' | 'warn' | 'danger';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private messageDeletionTime = 3500;
  private notificationSource = new Subject<Notification>();

  notificationState = this.notificationSource.asObservable();

  changeNotification(content: string, style: Severities) {
      const notification = new Notification(content, style);

      if (content) {
          this.notificationSource.next(notification);
      }

      setTimeout(() => {
          this.dissmissNotifaction();
      }, this.messageDeletionTime);
  }

  dissmissNotifaction() {
      this.notificationSource.next();
  }
}
