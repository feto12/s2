import { Component, OnInit, Input } from '@angular/core';
import { NotificationService } from './notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent {
  
  @Input() notification: Notification;

  constructor(private notificationService: NotificationService) { }

  
  public dissmissNotification() {
      this.notificationService.dissmissNotifaction();
  }
}
