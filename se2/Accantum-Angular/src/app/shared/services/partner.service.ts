import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Configuration } from 'src/app/app.constants';
import { Partner } from 'src/app/shared/models/partner';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  private apiUrl = Configuration.apiUrl;

  constructor(private loginService: LoginService, private http: HttpClient) { }


  /** GET admin from the server */
  getPartners(): Observable<Partner[]> {
    return this.http.get<Partner[]>(`${this.apiUrl}Users`)
      .pipe(
        catchError(this.handleError)
      );
  }
  // Get a specific admin
  getPartnerByID(id: number): Observable<Partner> {
    const url = `${this.apiUrl}Users/${id}`;
    return this.http.get<Partner>(url).pipe(catchError(this.handleError));
    // Get Admin by ID
  }


  // add a new admin
  createPartner(partner: Partner): Observable<Partner> {
    return this.http.post<Partner>(`${this.apiUrl}Users/register`, { ...partner, creatorId: this.loginService.getCurrentUser().id })
      .pipe(catchError(this.handleError));
  }

  // update data of a specific admin
  updatePartnerByID(partner: Partner, id: number): Observable<Partner> {
    const url = `${this.apiUrl}Users/${id}`;
    return this.http.put<Partner>(url, partner).pipe(catchError(this.handleError));
  }

  // block admin
  blockPartner(partner: Partner, partnerID: number, blocked: boolean): Observable<Partner> {
    const url = `${this.apiUrl}Users/${partnerID}`;
    return this.http.put<Partner>(url, partner).pipe(catchError(this.handleError));
  }

  // delete a specific admin
  deletePartnerByID(id: number): Observable<Partner> {
    const url = `${this.apiUrl}Users/${id}`;
    return this.http.delete<Partner>(url).pipe(catchError(this.handleError));
  }



  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side error 
      console.error('An error occurred:', error.error.message);
    } else {
      //A server-side error
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  };

  /** DELETE: delete the Admin from the server. */
  deletePartner(partner: Partner | number): Observable<Partner> {
    const id = typeof partner === 'number' ? partner : partner.id;
    const url = `${this.apiUrl}Users/${id}`;

    return this.http.delete<Partner>(url, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
}





