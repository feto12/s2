import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Configuration } from 'src/app/app.constants';
import { License } from 'src/app/shared/models/license';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';

@Injectable({
  providedIn: 'root'
})
export class LicenseService {

  
  private apiUrl = Configuration.apiUrl;

  constructor(private loginService: LoginService, private http: HttpClient) { }
  

  /** GET license from the server */
  getLicenses(): Observable<License[]> {
    return this.http.get<License[]>(`${this.apiUrl}Users`)
    .pipe(
      catchError(this.handleError)
    );
  }
  //Get a specific license
  getLicenseByID(id: number): Observable<License>{
    const url = `${this.apiUrl}Users/${id}`;
  return this.http.get<License>(url).pipe(catchError(this.handleError));
  }
  
  
  //add a new license
  createLicense(license: License): Observable<License>{
    return this.http.post<License>(`${this.apiUrl}Users/register`, { ...license, creatorId: this.loginService.getCurrentUser().id })
    .pipe(catchError(this.handleError));
  }
  
  //update data of a specific license
  updateLicenseByID(license: License, id: number): Observable<License> {
    const url = `${this.apiUrl}Users/${id}`;
    return this.http.put<License>(url, license).pipe(catchError(this.handleError));
  }
  
  //delete a specific license
  deleteLicenseByID(id: number): Observable<License>{
    const url = `${this.apiUrl}Users/${id}`;
    return this.http.delete<License>(url).pipe(catchError(this.handleError));
  }
  
  
  
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side error 
      console.error('An error occurred:', error.error.message);
    } else {
      //A server-side error
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  };
  
  /** DELETE: delete the license from the server. 
  deleteLicense (license: License | number): Observable<License> {
    const id = typeof license === 'number' ? license : license.id;
    const url = `${this.apiUrl}Users/${id}`;
  
    return this.http.delete<License>(url, httpOptions).pipe(
      catchError(this.handleError)
    );
  }
*/


}
