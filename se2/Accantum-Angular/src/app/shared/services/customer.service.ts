import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Configuration } from 'src/app/app.constants';
import { Customer } from '../models/customer.model';
import { LoginService } from 'src/app/login/login.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private apiUrl = Configuration.apiUrl;

  constructor(private loginService: LoginService, private http: HttpClient) { }

  /** GET Customer from the server */
  getCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${this.apiUrl}Users`)
      .pipe(
        catchError(this.handleError)
      );
  }
  // Get a specific Customer
  getCustomerByID(customerId: number): Observable<Customer> {
    const url = `${this.apiUrl}Users/${customerId}`;
    return this.http.get<Customer>(url).pipe(catchError(this.handleError));
  }


  // add a new Customer
  createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`${this.apiUrl}Users/register`, { ...customer, creatorId: this.loginService.getCurrentUser().id })
      .pipe(catchError(this.handleError));
  }

  // update data of a specific Customer
  updateCustomerByID(Customer: Customer, CustomerID: number): Observable<Customer> {
    const url = `${this.apiUrl}Users/${CustomerID}`;
    return this.http.put<Customer>(url, Customer).pipe(catchError(this.handleError));
  }

  // block customer
  blockCustomer(customer: Customer, customerID: number, blocked: boolean): Observable<Customer> {
    const url = `${this.apiUrl}Users/${customerID}`;
    return this.http.put<Customer>(url, customer).pipe(catchError(this.handleError));
  }

  // delete a specific Customer
  deleteCustomerByID(CustomerID: number): Observable<Customer> {
    const url = `${this.apiUrl}Users/${CustomerID}`;
    return this.http.delete<Customer>(url).pipe(catchError(this.handleError));
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side error 
      console.error('An error occurred:', error.error.message);
    } else {
      // A server-side error
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  };

  /** DELETE: delete the hero from the server. */
  deleteCustomer(Customer: Customer | number): Observable<Customer> {
    const id = typeof Customer === 'number' ? Customer : Customer.id;
    const url = `${this.apiUrl}Users/${id}`;

    return this.http.delete<Customer>(url, httpOptions).pipe(
      catchError(this.handleError)
    );
  }



}
