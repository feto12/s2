import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap, retry } from 'rxjs/operators';
import { Configuration } from 'src/app/app.constants';

//Klasse Admin liefert die Attribute
import { Admin } from '../models/admin';
import { User } from 'src/app/shared/models/user.model';

import { Router } from '@angular/router';
import { LoginService } from 'src/app/login/login.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  
  private apiUrl = Configuration.apiUrl;
  

  //The admin web Api expects a special header in HTTP save requests
  //That header is in the httpOptions constant defined in the AdminService
  /*const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  */
 
  constructor(private loginService: LoginService, private http: HttpClient) { }
  

/** GET admin from the server */
getAdmins(): Observable<Admin[]> {
  return this.http.get<Admin[]>(`${this.apiUrl}Users`)
  .pipe(
    catchError(this.handleError)
  );
}
//Get a specific admin
getAdminByID(adminID: number): Observable<Admin>{
  const url = `${this.apiUrl}Users/${adminID}`;
return this.http.get<Admin>(url).pipe(catchError(this.handleError));
//Get Admin by ID 
}


//add a new admin
createAdmin(admin: Admin): Observable<Admin>{
  return this.http.post<Admin>(`${this.apiUrl}Users/register`, { ...admin, creatorId: this.loginService.getCurrentUser().id })
  .pipe(catchError(this.handleError));
}

//update data of a specific admin
updateAdminByID(admin: Admin, adminID: number): Observable<Admin> {
  const url = `${this.apiUrl}Users/${adminID}`;
  return this.http.put<Admin>(url, admin).pipe(catchError(this.handleError));
}
//block admin
blockAdmin(admin: Admin, adminID: number, blocked: boolean): Observable<Admin> {
  const url = `${this.apiUrl}Users/${adminID}`;
  return this.http.put<Admin>(url, admin).pipe(catchError(this.handleError));
}

//delete a specific admin
deleteAdminByID(adminID: number): Observable<Admin>{
  const url = `${this.apiUrl}Users/${adminID}`;
  return this.http.delete<Admin>(url).pipe(catchError(this.handleError));
}



private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side error 
    console.error('An error occurred:', error.error.message);
  } else {
    //A server-side error
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  return throwError('Something bad happened; please try again later.');
};

/** DELETE: delete the Admin from the server. */
deleteAdmin (admin: Admin | number): Observable<Admin> {
  const id = typeof admin === 'number' ? admin : admin.id;
  const url = `${this.apiUrl}Users/${id}`;

  return this.http.delete<Admin>(url, httpOptions).pipe(
    catchError(this.handleError)
  );
}
}





