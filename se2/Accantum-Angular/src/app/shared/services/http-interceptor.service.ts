import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { LoginService } from 'src/app/login/login.service';
import { NotificationService } from '../components/notification/notification.service';


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

  constructor(
      private notificationService: NotificationService,
      private injector: Injector
  ) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      const authService = this.injector.get(LoginService);
      let tokenizedRequest = request;
      const currentUserToken = authService.getToken();

      if (!request.url.includes('/login') && currentUserToken !== '') {
          tokenizedRequest = request.clone({
              setHeaders: {
                  Authorization: 'Bearer ' + currentUserToken
              }
          });
      }

      return next.handle(tokenizedRequest)
          .pipe(
              catchError((error: HttpErrorResponse) => {
                  let errorMessage;
                  errorMessage = error.error.message;

                  this.notificationService.changeNotification(errorMessage, 'danger');
                  return throwError(errorMessage);
              })
          )

  }
}
