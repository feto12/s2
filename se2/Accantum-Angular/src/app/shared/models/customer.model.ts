
export class Customer {

  id: number;
  firstName: string;
  lastName: string;
  password: string;

  street: string;

  city: string;
  zip: number;
  eMail: string;
  phone: string;
  Housenumber: number;
  role: string;
  blocked: boolean;
  creatorId: number;
}
