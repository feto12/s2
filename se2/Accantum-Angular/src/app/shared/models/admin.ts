export class Admin {

  id: number;
  FirstName: string;
  LastName: string;
  Password: string;

  Street: string;
  City: string;
  ZIP: number;

  EMail: string;
  Phone: string;
  Housenumber: number;
  role: string;
  blocked: boolean;

  creatorId: number;
}
