import { DecimalPipe } from '@angular/common';

export class License {
       
       
    id: number;
    lastName: string; 
    firstName: string;
    salutation: string;
    phone: string;
    mobile: string;
    fax: string; 
    email1: string; 
    email2: string;  

    assignPartner: number; 
    company: string; 
    country: string; 
    street: string; 
    city: string;
    zip: number; 
    matchCode: string; 
    accantumNr: number; 
    homepage: string; 
    department: string; 
    AprvNumber: string; 
  
  
    language: string;
    description: string; 
    productCode: string;
    percent: string; 
    size: string;
    outputFormat: string; 
    lightUser: number;  
    fullUser: number;  
    totalUser: number;  
 
    comment: string; 
    accComment: string; 
    admComment: string; 
    receiptDate: Date; 
    receiptNumber: number;
    recordedDate: Date; 
    creatorUser: number; 
    changeDate: Date; 
    expiryDate: Date; 
    yearEnd: number; 
    licenseInfo: string; 
    monthEnd: number;
    sn: string; 
    AbbyyAkgState: string;
    AbbyyComment: string;
    AbbyyErgNr: number; 
    AbbyyLicenseKind: string;
    AbbyyVersion: number;
    AbbyyAkgYearEnd: number;
    AbbyyAkgMonthEnd: number; 
    AbbyyRetailerPurchasePrice: string;
    AbbyyKey: string; 
    AbbyyLicenseSize: number;
    AbbyyLicenseType: string; 
    state: string; 
}