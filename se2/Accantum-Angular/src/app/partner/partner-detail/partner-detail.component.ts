import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PartnerService } from 'src/app/shared/services/partner.service';
import { Partner } from 'src/app/shared/models/partner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-partner-detail',
  templateUrl: './partner-detail.component.html',
  styleUrls: ['./partner-detail.component.css']
})
export class PartnerDetailComponent implements OnInit {
 
  @Input() partner: Partner;
  form: FormGroup;
  btn: string;
  constructor(
    private route: ActivatedRoute,
    private partnerService: PartnerService,
    private location: Location,
    private formBuilder: FormBuilder,
  ) { }

  

  ngOnInit() :void {
    this.getPartner(); 
  
    }

  //Show detail of an specific partner
  getPartner(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.partnerService.getPartnerByID(id)
    .subscribe(partner => this.partner = partner);
  
  }
  //go to the previous page
  goBack(): void {
    this.location.back();
  }
  //change the Data of an specific partner
  save(): void {
    this.partnerService.updatePartnerByID(this.partner, this.partner.id)
    .subscribe(() => this.goBack());
  }

   //block admin 
   blockPartner(): void {
     if(this.partner.blocked == false){
    this.partnerService.blockPartner(this.partner, this.partner.id, this.partner.blocked =true )
    .subscribe(() => console.log(this.partner.blocked) );
    
     }else{
      this.partnerService.blockPartner(this.partner, this.partner.id, this.partner.blocked =false )
      .subscribe(() => console.log(this.partner.blocked) );
     }

  }
 

  blockStatus(btn): void {
    if(this.partner.blocked == false){
       btn.value= "Sperren"; 
     }else{
       btn.value= "Entsperren";
     }
  }
  
 

  deletePartnerByID(id: number){
    return this.partnerService.deletePartner(id)
    .subscribe((partner:Partner) => {
      console.log('Partner deleted', partner, this.goBack());
    },
    error  => {
      console.log("Error", error);
    });
  }  

}
