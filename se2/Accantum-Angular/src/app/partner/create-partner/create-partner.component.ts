import { Component, OnInit } from '@angular/core';
import { Partner } from 'src/app/shared/models/partner';
import { PartnerService } from 'src/app/shared/services/partner.service';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/user/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-create-partner',
  templateUrl: './create-partner.component.html',
  styleUrls: ['./create-partner.component.css']
})
export class CreatePartnerComponent implements OnInit {

  form: FormGroup;
  partners: Partner[];
  partner: Partner;
  Role = "Partner"; 

  constructor(
    private partnerservice: PartnerService, 
    private formBuilder: FormBuilder,
    private location: Location
    ) { }

  ngOnInit() {
  
    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      lastName: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      password: ['', Validators.required],
      street: ['',[Validators.required,Validators.pattern(/^[a-zA-Z0-9ßöäü.]+$/ ) ]],
      city: ['', [Validators.required,Validators.pattern(/^[a-zA-Z]+$/ ) ]],
      zip: ['', [Validators.required, Validators.minLength(4)]],
      eMail: ['', [Validators.required, Validators.pattern(  /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/ )] ],
      phone: ['', [Validators.required, Validators.pattern( /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]],
      role: [ this.Role ]
    
    });
  }

    
  //Post Partner
  createNewPartner(partner: Partner){
    partner = this.form.value;
    return this.partnerservice.createPartner(partner)
    .subscribe((data) => {
      console.log('New Partner created', this.goBack() );
    },
    error  => {
      console.log("Error", error);
    });
  }

  goBack(): void {
    this.location.back();
  }
}

