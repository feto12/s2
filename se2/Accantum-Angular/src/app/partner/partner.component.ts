import { Component, OnInit } from '@angular/core';
import { Partner } from 'src/app/shared/models/partner';
import { PartnerService } from 'src/app/shared/services/partner.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService, CurrentUser } from '../login/login.service';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.css']
})
export class PartnerComponent implements OnInit {

  form: FormGroup;
  partners: Partner[];
  partner: Partner;
  currentUser: CurrentUser;


  constructor(private loginService: LoginService, private partnerService: PartnerService, private formBuilder: FormBuilder) { 
    this.currentUser = loginService.getCurrentUser();
  }

  ngOnInit() {

    this.getPartners();
  }

  // Get all Partners
  getPartners() {
    const currentUser = this.loginService.getCurrentUser();
    const isAdminUser = ['Partner', 'Customer'].indexOf(currentUser.role) === -1;
    return this.partnerService.getPartners()
      .subscribe((data: Partner[]) => {
        this.partners = data;
        console.log(this.partners);
        // Iteriere über das Array und füge nur User mit Rolle = Partner ein!
        this.partners = this.partners.filter(rollePartner =>
          rollePartner.role === 'Partner'
          && (isAdminUser || rollePartner.id === currentUser.id || rollePartner.creatorId === currentUser.id)
        );
      }, error => {
        console.log('Error', error);
      });
  }

  // Get Partner by id
  getPartnerByID(Id: number) {
    return this.partnerService.getPartnerByID(Id)
      .subscribe((partner: Partner) => {
        this.partner = partner;
      },
        error => {
          console.log('Error', error);
        });
  }

}
