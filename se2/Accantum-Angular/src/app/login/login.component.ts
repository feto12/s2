import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { LoginService } from './login.service';
import { first } from 'rxjs/operators';
import { NotificationService } from '../shared/components/notification/notification.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;      
  error: boolean = false;

  constructor(
    private fb: FormBuilder,
     private auth: LoginService,
      private router: Router,
      private notificationService: NotificationService
      ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  mockLogin(){
    if(this.auth.mockLogin(this.loginForm)){
      this.auth.isLogin = true;
      this.router.navigate(['']);
  }
}

loginUser() {
  let userData;
  if (this.loginForm.invalid) {
      return;
  }

  userData = this.loginForm.value;

  this.auth
      .login(userData)
      .pipe(first())
      .subscribe(
          response => {
              this.router.navigate(['']);
              this.notificationService.changeNotification('Der Login war erfolgreich!', 'success');
          },
          error => {
            this.error = true;
          }
      );
}

}
