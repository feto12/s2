import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Configuration } from 'src/app/app.constants';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user.model';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';


export interface CurrentUser {
  token: string;
  id: number;
  role: 'Customer' | 'Admin' | 'Partner';
}


@Injectable({
  providedIn: 'root'
})

export class LoginService {

  private apiUrl = Configuration.apiUrl;
  form: FormGroup;
  isLogin: boolean = false;
  private currentUser: CurrentUser;

  constructor(
    private http: HttpClient,
      private router: Router,
      private fb: FormBuilder
  ) { }

  login(user: User) {
    localStorage.setItem('email', user.email);
      return this.http.post<any>(`${this.apiUrl}Users/authenticate`, { email: user.email, password: user.password })
          .pipe(map(user => {
              if (user) {
                  localStorage.setItem('currentUser', JSON.stringify(user));
                  this.currentUser = user;
              }
              return user;
          }));
  }

  isAuthenticated() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')) as CurrentUser;
    console.log(this.currentUser);
    return !!localStorage.getItem('currentUser');
}

  getCurrentUser() {
    if (this.currentUser) {
      return this.currentUser;
    }
    this.currentUser = JSON.parse(localStorage.getItem('currentUser')) as CurrentUser;
    return this.currentUser;
  }

  getToken() {
    const currentUser: CurrentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
        this.currentUser = currentUser;
        return currentUser.token;
    }
    return '';
}

  getAdmin(){
    return this.http.get(this.apiUrl);
  }

  createAdmin(admin: any){
    return this.http.post(this.apiUrl, admin);
  }

  modifyAdmin(admin: any){
    return this.http.put(this.apiUrl, admin);
  }



  
  mockLogin(form){
    if(form.value.username == 'admin' && form.value.password == 'password'){
      return true;
    }
    return false;
}

}
