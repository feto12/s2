import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public static apiUrl = 'https://localhost:5001/swagger';
}
