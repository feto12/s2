import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  constructor(private auth: AuthentificationService) { }

  ngOnInit() {
  }

  getAdmin(){
   return this.auth.getAdmin().subscribe((data) => {
      console.log(data);
    });
  }
}
