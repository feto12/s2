import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  

  license: string = 'license';
  constructor(private auth: AuthentificationService) { }

  ngOnInit() {
  
  }
  
  
}
