import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LizenzenComponent } from './lizenzen.component';

describe('LizenzenComponent', () => {
  let component: LizenzenComponent;
  let fixture: ComponentFixture<LizenzenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LizenzenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LizenzenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
