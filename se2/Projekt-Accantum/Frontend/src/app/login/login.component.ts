import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { AuthentificationService } from '../authentification.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;       
  

  constructor(private fb: FormBuilder, private auth: AuthentificationService, private router: Router) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
 
    login(){
      if(this.auth.login(this.form)){
        this.auth.isLogin = true;
        this.router.navigate(['home']);
      }
    }

    }

  
 


