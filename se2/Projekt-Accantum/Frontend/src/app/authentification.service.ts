import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  url = 'http://localhost:3000/'
  urlAdmin = 'http://localhost:3000/admin/'
  form: FormGroup;
  isLogin: boolean = false;

  constructor(private http: HttpClient, private fb: FormBuilder) { }


  ngOnInit() {
  
  }
 

  getAdmin(){
    return this.http.get(this.url);
  }

  createAdmin(admin: any){
    return this.http.post(this.url, admin);
  }

  modifyAdmin(admin: any){
    return this.http.put(this.url, admin);
  }













  login(form){
    if(form.value.username == 'admin' && form.value.password == 'password'){
      return true;
    }
    return false;
}
}
