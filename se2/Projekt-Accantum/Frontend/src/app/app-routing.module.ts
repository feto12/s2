import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { PartnerComponent } from './partner/partner.component';
import { KundeComponent } from './kunde/kunde.component';
import { LizenzenComponent } from './lizenzen/lizenzen.component';
import { LogoutComponent } from './logout/logout.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
{path: '', component: LoginComponent},
{path: 'home', component: HomeComponent},
{path: 'admin', component: AdminComponent},
{path: 'partner', component: PartnerComponent},
{path: 'kunde' , component: KundeComponent},
{path: 'lizenzen', component: LizenzenComponent},
{path: 'logout', component: LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
