﻿**README - SOFTWAREPROJEKT LICENSE**<br>
**ACCANTUM-SE2:**

<a href="https://inf-git.fh-rosenheim.de/se2-wif-19/license/wikis/uploads/b98262ecf37991faa5506f34a3ad1046/logo-accantum-web.png"><img src="https://inf-git.fh-rosenheim.de/se2-wif-19/license/wikis/uploads/b98262ecf37991faa5506f34a3ad1046/logo-accantum-web.png" alt="Logo_Accantum" /></a>
<a href="https://inf-git.fh-rosenheim.de/se2-wif-19/license/wikis/uploads/img/one2.jpg"><img src="https://inf-git.fh-rosenheim.de/se2-wif-19/license/wikis/uploads/img/one2.jpg" alt="Logo_License" /></a>

Das Team License entwickelt für die Accantum GmbH eine Individualsoftware.<br>
Die Software soll die Lizenzerwaltung für die DMS-/WFMS-Software der Accantum GmbH vereinfachen.<br>
Ziel ist es die Prozesse der Accantum GmbH mit einer Software zu unterstützen.<br>
Hierzu wird ein Prototyp entwickelt.<br>
Einen Überblick über die Software (und -entwicklung) schafft das angelegte Wiki.

[Die Home Seite des Wiki finden Sie hier!](https://inf-git.fh-rosenheim.de/se2-wif-19/license/wikis/home)

Auf dieser Home Seite wird ein Überblick der Software geschaffen und zu den einzelnen detallierten Unterpunkten verlinkt.
